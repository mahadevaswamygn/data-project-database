# data-project-database

## Create DB, permissions etc.
CREATE USER 'mahadeva'@'localhost'  IDENTIFIED BY 'adminuser';
CREATE DATABASE  IPL_project;
GRANT ALL PRIVILEGES ON DATABASE IPL_project_database TO manju; 

##  Clean up script
DROP DATABASE IPL_project;
DROP USER mahadeva;

## Load CSV
Here two ways to load csv files data into tables.
### - Run script
### - Following steps

- Run script.
1. First run the CreateTables.sql file.
2. Second run loadCSVFileDataIntoTable.sql file.

- Following steps,

  Here some of steps to load CSV file:
1. Create database with name "IPL_project" in MySQL workbench.
2. Go to that database using "USE IPL_project;".
3. Create table for matches using : CREATE TABLE matches(id INT,
 season varchar(10),
 city VARCHAR(30),
 dates VARCHAR(20),
 team1 VARCHAR(100), 
 team2 VARCHAR(100),
 toss_winner VARCHAR(100),
 toss_decision VARCHAR(100),
 result VARCHAR(50),
 dl_applied VARCHAR(50),
 winner VARCHAR(50),
 win_by_runs INT,
 win_by_wickets INT ,
 player_of_match VARCHAR(100),
 venue VARCHAR(200),
 umpire1 VARCHAR(40),
 umpire2 VARCHAR(40),
 umpire3 VARCHAR(40));
 
 4. Create table for deliveries :CREATE TABLE deliveries(match_id INT,
inning INT,
batting_team VARCHAR(50),
bowling_team VARCHAR(50),
overs INT,
ball INT,
batsman VARCHAR(50),
non_striker VARCHAR(50),
bowler VARCHAR(50),
is_super_over INT,
wide_runs INT,
bye_runs INT,
legbye_runs INT,
noball_runs INT,
penalty_runs INT,
batsman_runs INT,
extra_runs INT,
total_runs INT,
player_dismissed VARCHAR(40),
dismissal_kind VARCHAR(40),
fielder VARCHAR(40));

5. After Created two table Load the csv file to tabale using below script.
Change the csv file path. Provide exact csv file path where your csv file located in your computer.
LOAD DATA INFILE '/var/lib/mysql-files/matches.csv'
INTO TABLE matches
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(id,season,city,dates,team1,team2,toss_winner,toss_decision,result,dl_applied,winner,win_by_runs,win_by_wickets,	player_of_match,venue,umpire1,umpire2,umpire3);

LOAD DATA INFILE '/var/lib/mysql-files/deliveries.csv'
INTO TABLE deliveries
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(match_id,inning,batting_team,bowling_team,overs,ball,batsman,non_striker,bowler,is_super_over,wide_runs,bye_runs,legbye_runs,noball_runs,penalty_runs,batsman_runs,extra_runs,total_runs,player_dismissed,dismissal_kind,fielder);

## Solve the IPL problems
- Run the iplProjectQueries.sql file. 







